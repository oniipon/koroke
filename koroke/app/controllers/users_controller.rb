class UsersController < ApplicationController
  def index
   @users = User.all
  end

  def show
  end

  def new
  end
  
  def create
    @user = User.new
    @user.name = params[:user][:name]
    @user.count = params[:user][:count]
    @user.save
    redirect_to '/users/index'
  end
end
